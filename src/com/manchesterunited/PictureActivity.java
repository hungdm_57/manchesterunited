package com.manchesterunited;

import helper.ServiceHandler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.ListPlayer;
import models.Photos;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import adapter.PictureAdapter;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class PictureActivity extends ActionBarActivity {
	private List<Photos>arr=new ArrayList<Photos>();
	ListView lvPhoto;
	PictureAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_picture);
		final ProgressDialog progress = new ProgressDialog(this);
	    progress.setTitle("Loading");
	    progress.setMessage("Please wait...");
	    progress.show();

	    Runnable progressRunnable = new Runnable() {

	        @Override
	        public void run() {
	            progress.cancel();
	        }
	    };

	    Handler pdCanceller = new Handler();
	    pdCanceller.postDelayed(progressRunnable, 5000);

		arr=getPhotos("1");
		adapter=new PictureAdapter(this, R.layout.listview_picture, arr);
		lvPhoto=(ListView)findViewById(R.id.lvPicture);
		
		lvPhoto.setAdapter(adapter);
	}
	private List<Photos> getPhotos(String page) {
		// TODO Auto-generated method stub
		List<Photos>arr=new ArrayList<Photos>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		Photos photo=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlPhotos, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String name=obj.getString("name");
					String date=obj.getString("date");
					SimpleDateFormat sdf=new SimpleDateFormat(MainActivity.OLD_FORMAT);
					Date d=sdf.parse(date);
					sdf.applyPattern(MainActivity.NEW_FORMAT);
					String newDate=sdf.format(d);
					String url=obj.getString("url");
					photo=new Photos(id, name,newDate,url);
					arr.add(photo);
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.picture, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
