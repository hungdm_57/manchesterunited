package com.manchesterunited;

import helper.ServiceHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Match;
import models.News;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import adapter.MatchsAdapter;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class KetQuaThiDau extends ActionBarActivity {
	private List<Match> arrMatchs=new ArrayList<Match>();
	MatchsAdapter adapter_matchs;
	ListView lvMatch;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ket_qua_thi_dau);
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		arrMatchs=getMatchs("1", "1");
		adapter_matchs=new MatchsAdapter(this, R.layout.listview_lich_thi_dau, arrMatchs);
		lvMatch=(ListView)findViewById(R.id.lvPremier);
		lvMatch.setAdapter(adapter_matchs);
		lvMatch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String home=arrMatchs.get(position).getHome();
				String visitor=arrMatchs.get(position).getVisitor();
				String result=arrMatchs.get(position).getResult();
				gotoChiTietTranDau(home,visitor,result,position);
			}
		});
	}
	protected void gotoChiTietTranDau(String home, String visitor, String result,int position) {
		// TODO Auto-generated method stub
		Bundle b=new Bundle();
		b.putString("HOME", home);
		b.putString("VISTOR", visitor);
		b.putString("RESULT", result);
		b.putInt("POSITION", position);
		Intent i=new Intent(this, ChiTietTranDau.class);
		i.putExtra("DATA", b);
		startActivity(i);
	}

	private List<Match> getMatchs(String page, String type) {
		// TODO Auto-generated method stub
		List<Match>arr=new ArrayList<Match>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		params.add(new BasicNameValuePair("type", type));
		Match match=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlMatch, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String league=obj.getString("league");
					String round=obj.getString("round");
					String home=obj.getString("home\t");
					String visitor=obj.getString("visitor");
					String city=obj.getString("city");
					String stadium=obj.getString("stadium");
					String time=obj.getString("time");
					String result=obj.getString("result");
					String date=obj.getString("date");
					SimpleDateFormat sdf=new SimpleDateFormat(MainActivity.OLD_FORMAT);
					Date d=sdf.parse(date);
					sdf.applyPattern(MainActivity.NEW_FORMAT);
					String newDate=sdf.format(d);
					match=new Match(id, league, round, home, visitor, city, stadium, newDate, time, result);
					arr.add(match);
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ket_qua_thi_dau, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
