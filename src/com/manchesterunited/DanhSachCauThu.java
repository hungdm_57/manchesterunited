package com.manchesterunited;

import helper.ServiceHandler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import models.ListPlayer;
import models.News;
import adapter.PlayerAdapter;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class DanhSachCauThu extends ActionBarActivity {
	private List<ListPlayer>arrPlayer=new ArrayList<ListPlayer>();
	ListView lvPlayer;
	PlayerAdapter adapter_players;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_danh_sach_cau_thu);
		ProgressDialog pDialog=new ProgressDialog(this);
		final ProgressDialog progress = new ProgressDialog(this);
	    progress.setTitle("Loading");
	    progress.setMessage("Please wait...");
	    progress.show();

	    Runnable progressRunnable = new Runnable() {

	        @Override
	        public void run() {
	            progress.cancel();
	        }
	    };

	    Handler pdCanceller = new Handler();
	    pdCanceller.postDelayed(progressRunnable, 5000);

		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		lvPlayer=(ListView)findViewById(R.id.lvPlayer);
		arrPlayer=getPlayers("1", "1");
		adapter_players=new PlayerAdapter(this, R.layout.listview_danh_sach_cau_thu, arrPlayer);
		lvPlayer.setAdapter(adapter_players);
		lvPlayer.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				gotoChiTietCauThu();
			}
		});
		
	}
	protected void gotoChiTietCauThu() {
		// TODO Auto-generated method stub
		Intent i =new Intent(this, ChiTietCauThu.class);
		startActivity(i);
	}
	private List<ListPlayer> getPlayers(String page, String type) {
		// TODO Auto-generated method stub
		List<ListPlayer>arr=new ArrayList<ListPlayer>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		params.add(new BasicNameValuePair("type", type));
		ListPlayer player=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlPlayer, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String number=obj.getString("number");
					String url=obj.getString("image");
					String firstName=obj.getString("firstname");
					player=new  ListPlayer(id, url, firstName, number);
					arr.add(player);
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}
	public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) newWidth) / width;
	    float scaleHeight = ((float) newHeight) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    // RESIZE THE BIT MAP
	    matrix.postScale(scaleWidth, scaleHeight);

	    // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	    return resizedBitmap;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.danh_sach_cau_thu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
