package com.manchesterunited;

import helper.ServiceHandler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Photos;
import models.SuKien;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import adapter.SukienAdapter;
import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class SuKienLichSu extends ActionBarActivity {
	private List<SuKien>arr=new ArrayList<SuKien>();
	ListView lvSukien;
	SukienAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_su_kien_lich_su);
		lvSukien=(ListView)findViewById(R.id.lvSuKien);
		arr=getSukien("1");
		adapter=new SukienAdapter(this, R.layout.listview_su_kien, arr);
		lvSukien.setAdapter(adapter);
	}

	private List<SuKien> getSukien(String page) {
		// TODO Auto-generated method stub
		List<SuKien>arr=new ArrayList<SuKien>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		SuKien sukien=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlHistory, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String title=obj.getString("title");
					String date=obj.getString("date");
					SimpleDateFormat sdf=new SimpleDateFormat(MainActivity.OLD_FORMAT);
					Date d=sdf.parse(date);
					sdf.applyPattern(MainActivity.NEW_FORMAT);
					String newDate=sdf.format(d);
					sukien=new SuKien(title, id, newDate);
					arr.add(sukien);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.su_kien_lich_su, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
