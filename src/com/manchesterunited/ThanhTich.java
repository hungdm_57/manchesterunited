package com.manchesterunited;

import helper.ServiceHandler;

import java.net.URL;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Archievement;
import models.ListPlayer;
import models.Photos;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import adapter.ArchieveAdapter;
import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class ThanhTich extends ActionBarActivity {
	private List<Archievement>arr=new ArrayList<Archievement>();
	ListView lvThanhTich;
	ArchieveAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thanh_tich);
		arr=getArchieve("1");
		arr.addAll(getArchieve("2"));
		adapter=new ArchieveAdapter(this, R.layout.listview_thanh_tich, arr);
		lvThanhTich=(ListView)findViewById(R.id.lvThanhTich);
		lvThanhTich.setAdapter(adapter);
	}
	private List<Archievement> getArchieve(String page) {
		// TODO Auto-generated method stub
		List<Archievement>arr=new ArrayList<Archievement>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		Archievement arc=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlArchieve, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String name=obj.getString("league_name");
					String date=obj.getString("date");
					SimpleDateFormat sdf=new SimpleDateFormat(MainActivity.OLD_FORMAT);
					String des=obj.getString("describle");
					Date d=sdf.parse(date);
					sdf.applyPattern(MainActivity.NEW_FORMAT);
					String newDate=sdf.format(d);
					arc=new Archievement(id, name, newDate, des);
					arr.add(arc);
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.thanh_tich, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
