package com.manchesterunited;

import java.util.ArrayList;
import java.util.List;

import models.Charts;
import adapter.ChartsAdapter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class BangXepHang extends ActionBarActivity {
	private List<Charts>arr =new ArrayList<Charts>();
	ChartsAdapter adapter;
	ListView lvCharts;
	Charts charts;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bang_xep_hang);
		charts=new Charts("1", "Chelsea", "36", "25", "9", "2", "70", "28", "84");
		arr.add(charts);
		charts=new Charts("2", "Manchester City", "37", "23", "7", "7", "81", "38", "76");
		arr.add(charts);
		charts=new Charts("3", "Arsenal", "36", "21", "8", "7", "67", "35", "71");
		arr.add(charts);
		lvCharts=(ListView)findViewById(R.id.lvCharts);
		adapter=new ChartsAdapter(this, R.layout.listview_bang_xep_hang, arr);
		lvCharts.setAdapter(adapter);
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bang_xep_hang, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
