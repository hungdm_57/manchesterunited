package com.manchesterunited;

import helper.ServiceHandler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.MatchDetail;
import models.Photos;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import adapter.MatchDetailAdapter;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ChiTietTranDau extends ActionBarActivity {
	TextView tvHome,tvResult,tvVisitor;
	private List<MatchDetail>arr=new ArrayList<MatchDetail>();
	MatchDetailAdapter adapter;
	ListView lvDetail;
	String result="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chi_tiet_tran_dau);
		Intent i=getIntent();
		tvHome=(TextView)findViewById(R.id.tvChiTietHome);
		tvResult=(TextView)findViewById(R.id.tvChiTietResult);
		tvVisitor=(TextView)findViewById(R.id.tvChiTietVisistor);
		
		Bundle b=i.getBundleExtra("DATA");
		String home=b.getString("HOME");
		String visitor=b.getString("VISTOR");
		String result="3-0";
		String position=b.getString("POSITION");
		tvHome.setText(home);
		tvVisitor.setText(visitor);
		tvResult.setText(result);
		arr=getMatchDetail("1", "1");
		lvDetail=(ListView)findViewById(R.id.lvChitiet);
		adapter=new MatchDetailAdapter(this, R.layout.listview_thong_tin_tran_dau,arr);
		lvDetail.setAdapter(adapter);
	}
	private List<MatchDetail> getMatchDetail(String page,String matchId) {
		// TODO Auto-generated method stub
		List<MatchDetail>arr=new ArrayList<MatchDetail>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		params.add(new BasicNameValuePair("id", matchId));
		MatchDetail md=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(MainActivity.urlMatchDetail, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				int h=0;
				int v=0;
				for(int i=0;i<json.length();i++){
					
					
					JSONObject obj=json.getJSONObject(i);
					String club_Id=obj.getString("club_id");
					String time=obj.getString("time");
					String player=obj.getString("player");
					String goal=obj.getString("goal");
					String card=obj.getString("card");
					String assist=obj.getString("assist");
					if(goal.equals("1")){
						if(club_Id.equals("1")){
							h=h+1;
						}
						else v=v+1;
						result=h+" - "+v;
						md=new MatchDetail(club_Id, time, player, result, assist);
						arr.add(md);		
					}
					else{
						md=new MatchDetail(club_Id, time, player, card);
						arr.add(md);
					}
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}
	private void showtoast(String string) {
		// TODO Auto-generated method stub
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chi_tiet_tran_dau, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
