package com.manchesterunited;




import helper.ServiceHandler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import models.ItemMenu;
import models.News;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.manchesterunited.R;
import com.manchesterunited.R.dimen;
import com.manchesterunited.R.drawable;
import com.manchesterunited.R.id;
import com.manchesterunited.R.layout;
import com.manchesterunited.R.menu;
import com.manchesterunited.R.string;

import adapter.MenuAdapter;
import adapter.NewsAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.R.anim;
import android.R.integer;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
public class MainActivity extends ActionBarActivity {
	public static final String urlNews="http://sentienich.aviostore.com/ManchesterUnited/news.php";
	public static final String urlPlayer="http://sentienich.aviostore.com/ManchesterUnited/player.php";
	public static final String urlMatch="http://sentienich.aviostore.com/ManchesterUnited/match.php";
	public static final String urlPhotos="http://sentienich.aviostore.com/ManchesterUnited/image.php";
	public static final String urlHistory="http://sentienich.aviostore.com/ManchesterUnited/history.php";
	public static final String urlArchieve="http://sentienich.aviostore.com/ManchesterUnited/achieve.php";
	public static final String urlMatchDetail="http://sentienich.aviostore.com/ManchesterUnited/match_detail.php";
	public static final String NEW_FORMAT = "dd-MM-yyyy";
	public static final String OLD_FORMAT = "yyyy-MM-dd";

	ImageView myImg;
	ListView lvItemMenu,lvNews;
	SlidingMenu menu;
	private List<ItemMenu> itemMenus=new ArrayList<ItemMenu>();
	private List<News> arrNews=new ArrayList<News>();
	MenuAdapter adapter;
	NewsAdapter adapter_news;
	int menuStatus=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		ActionBar mActionBar =getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		
		menu=new SlidingMenu(this);
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_with);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.1f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.custom_menu);
		lvItemMenu=(ListView)findViewById(R.id.lvItem);
		itemMenus.add(new ItemMenu(1, getResources().getString(R.string.news), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(2, getResources().getString(R.string.bang_xep_hang), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(3, getResources().getString(R.string.ket_qua), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(4, getResources().getString(R.string.cau_thu), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(5, getResources().getString(R.string.chuyen_nhuong), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(6, getResources().getString(R.string.photo), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(7, getResources().getString(R.string.thanh_tich), R.drawable.mu_logo));
		itemMenus.add(new ItemMenu(8, getResources().getString(R.string.lich_su), R.drawable.mu_logo));
		if(adapter==null){
			adapter=new MenuAdapter(this, itemMenus);
			lvItemMenu.setAdapter(adapter);
			lvItemMenu.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					int i= itemMenus.get(position).getId();
					switch (i) {
					case 1:
						gotoMain();
						break;
					case 2:
						goToBangXepHang();
						break;
					case 3:
						goToKetQua();
						break;
					case 4:
						gotoDanhSachCauThu();
						break;
					case 6:
						gotoPicture();
						break;
					case 7:
						gotoThanhTich();
						break;
					case 8:
						gotoSuKien();
						break;
					default:
						break;
					
					}
					
				}
			});
		}
		myImg=(ImageView)mCustomView.findViewById(R.id.ivMenu);
		myImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(menuStatus==1){
					menu.showMenu();
					menuStatus=0;
					
				} else
				if(menuStatus==0){
					menu.showContent();
					menuStatus=1;
				}
			}
		});
		arrNews=getNews("1","1");
		adapter_news=new NewsAdapter(this, R.layout.listview_tin_tuc, arrNews);
		lvNews=(ListView)findViewById(R.id.lvNew);
		lvNews.setAdapter(adapter_news);

		
	}

	protected void gotoThanhTich() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(this, ThanhTich.class);
		startActivity(intent);
	}

	protected void gotoSuKien() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, SuKienLichSu.class);
		startActivity(i);
	}

	protected void gotoPicture() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, PictureActivity.class);
		startActivity(i);
	}

	protected void gotoMain() {
		// TODO Auto-generated method stub
		menu.showContent();
	}

	private List<News> getNews(String page, String type) {
		// TODO Auto-generated method stub
		List<News>arr=new ArrayList<News>();
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("page", page));
		params.add(new BasicNameValuePair("type", type));
		News news=null;
		try {
			ServiceHandler serviceClient=new ServiceHandler();
			JSONArray json=serviceClient.makeServiceCall(urlNews, ServiceHandler.GET,params);
			if(json==null){
				showtoast("Không có dữ liệu");
			} else{
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					String id=obj.getString("id");
					String title=obj.getString("title");
					String content=obj.getString("content");
					String url= obj.getString("url");
					String date=obj.getString("date");
					SimpleDateFormat sdf=new SimpleDateFormat(OLD_FORMAT);
					Date d=sdf.parse(date);
					sdf.applyPattern(NEW_FORMAT);
					String newDate=sdf.format(d);
					String view=obj.getString("view");
					news=new News(id, title, content, newDate, view,url);
					arr.add(news);
				}
			}
		} catch (Exception e) {
			showtoast("Không có dữ liệu");
		}
		
		return arr;
	}

	protected void gotoDanhSachCauThu() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, DanhSachCauThu.class);
		startActivity(i);
	}

	protected void goToKetQua() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this, KetQuaThiDau.class);
		startActivity(i);
	}

	protected void goToBangXepHang() {
		// TODO Auto-generated method stub
		Intent i=new Intent(this,BangXepHang.class);
		startActivity(i);
	}

	protected void showtoast(String s) {
		// TODO Auto-generated method stub
		Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id==R.id.action_finish){
			finish();
		}
		return true;
	}
}
