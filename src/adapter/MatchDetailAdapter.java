package adapter;

import java.util.List;

import com.manchesterunited.R;

import models.MatchDetail;
import android.app.Activity;
import android.content.Context;
import android.database.CursorJoiner.Result;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MatchDetailAdapter extends ArrayAdapter<MatchDetail>{
	Activity context;
	int layoutId;
	List<MatchDetail>arr;
	public MatchDetailAdapter(Activity context, int resource,
			List<MatchDetail> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvPlayer,tvCard,tvAssist,tvResult;
		if(arr.get(position).getClubId().equals("1")){
			tvPlayer=(TextView)convertView.findViewById(R.id.tvItemHome);
			tvAssist=(TextView)convertView.findViewById(R.id.tvItemHomeAssist);
			tvPlayer.setText(arr.get(position).getPlayer());
			tvAssist.setText("Suarez");
		}
		if(!arr.get(position).getClubId().equals("1")){
			tvPlayer=(TextView)convertView.findViewById(R.id.tvItemVisistor);
			tvAssist=(TextView)convertView.findViewById(R.id.tvItemVisistorAssist);
			tvPlayer.setText(arr.get(position).getPlayer());
			tvAssist.setText("Red Player");
		}
		tvResult=(TextView)convertView.findViewById(R.id.tvItemResult);
		tvResult.setText(arr.get(position).getResult());
		return convertView;
	}
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}
}
