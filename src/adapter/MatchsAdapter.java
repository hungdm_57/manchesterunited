package adapter;

import java.util.List;
import com.manchesterunited.R;
import models.Match;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MatchsAdapter extends ArrayAdapter<Match>{
	Activity context;
	List<Match>arr;
	int layoutId;
	public MatchsAdapter(Activity context, int resource, List<Match> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.arr=objects;
		this.layoutId=resource;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvHome=(TextView)convertView.findViewById(R.id.tvHome);
		TextView tvVisitor=(TextView)convertView.findViewById(R.id.tvVisitor);
		TextView tvTime=(TextView)convertView.findViewById(R.id.tvTime);
		TextView tvResult=(TextView)convertView.findViewById(R.id.tvResult);
		tvHome.setText(arr.get(position).getHome()+"\n");
		tvVisitor.setText(arr.get(position).getVisitor()+"\n");
		tvTime.setText(arr.get(position).getDate()+"\n"+arr.get(position).getTime());
		tvResult.setText(tvResult.getText()+"\n"+"V�ng:"+arr.get(position).getRound());
		return convertView;
	}
}
