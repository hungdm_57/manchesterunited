package adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.manchesterunited.R;

import lazyloadImage.ImageLoader;
import models.ListPlayer;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PlayerAdapter extends ArrayAdapter<ListPlayer>{
	Activity context;
	int layoutId;
	List<ListPlayer> arr;
	public ImageLoader imgLoader;
	public PlayerAdapter(Activity context, int resource, List<ListPlayer> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
		imgLoader=new ImageLoader(context.getApplicationContext());
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		ImageView img=(ImageView)convertView.findViewById(R.id.imgPlayer);
		TextView tvPlayer=(TextView)convertView.findViewById(R.id.tvPlayer);
		tvPlayer.setText(arr.get(position).getNumber()+"-"+"Rooney");
		imgLoader.DisplayImage(arr.get(position).getImage(), img,70);
		
		return convertView;
	}
	
}
