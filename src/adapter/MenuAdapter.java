package adapter;

import java.util.List;

import com.manchesterunited.R;

import models.ItemMenu;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends BaseAdapter {
	private List<ItemMenu> menus;
	private Context context;
	private LayoutInflater mInflater;
	public MenuAdapter(Context context, List<ItemMenu> menus) {
		// TODO Auto-generated constructor stub
		this.menus = menus;
		this.context = context;
		mInflater = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return menus.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return menus.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view;
		ViewHolder holder;
		if(convertView==null){
			view=mInflater.inflate(R.layout.list_item_menu, parent, false);
			holder=new ViewHolder();
			holder.icon=(ImageView)view.findViewById(R.id.img_ic_sliding_menu);
			holder.name=(TextView)view.findViewById(R.id.tv_sliding_menu_name);
			view.setTag(holder);
		} else {
			view=convertView;
			holder=(ViewHolder)view.getTag();
		}
		ItemMenu item = menus.get(position);
		holder.icon.setImageResource(item.getIconRes());
		holder.name.setText(item.getNameString());
		holder.name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		return view;
	}
	private class ViewHolder {
		public ImageView icon;
		public TextView name;
		
	}
}
