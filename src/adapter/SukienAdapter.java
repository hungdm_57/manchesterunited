package adapter;

import java.util.List;

import com.manchesterunited.R;

import models.SuKien;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SukienAdapter extends ArrayAdapter<SuKien>{
	Activity contex;
	int resource;
	List<SuKien>arr;
	public SukienAdapter(Activity context, int resource, List<SuKien> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.contex=context;
		this.resource=resource;
		this.arr=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=contex.getLayoutInflater().inflate(resource, null);
		TextView title=(TextView)convertView.findViewById(R.id.tvSuKienTitle);
		TextView date=(TextView)convertView.findViewById(R.id.tvSuKienDate);
		title.setText(arr.get(position).getTitle());
		date.setText(arr.get(position).getdate());
		return convertView;
	}
}
