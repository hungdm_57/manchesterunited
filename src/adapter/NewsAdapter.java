package adapter;

import java.util.List;

import lazyloadImage.ImageLoader;
import models.News;

import com.manchesterunited.DanhSachCauThu;
import com.manchesterunited.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsAdapter extends ArrayAdapter<News>{
	Activity context;
	List<News> arr;
	int layoutId;
	public ImageLoader imageLoader;
	public NewsAdapter(Activity context, int resource, List<News> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
		imageLoader=new ImageLoader(context.getApplicationContext());
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvTitle=(TextView)convertView.findViewById(R.id.tvSuKienTitle);
		TextView tvDate=(TextView)convertView.findViewById(R.id.tvDate);
		TextView tvViews=(TextView)convertView.findViewById(R.id.tvView);
		TextView tvContent=(TextView)convertView.findViewById(R.id.tvContent);
		ImageView img=(ImageView)convertView.findViewById(R.id.imgTintuc);
		News newsObject=arr.get(position);
		tvTitle.setText(newsObject.getTitle());
		tvDate.setText(newsObject.getDate());
		tvViews.setText("view: "+newsObject.getViews());
		tvContent.setText(newsObject.getContent());
		imageLoader.DisplayImage(arr.get(position).getUrl(), img,200);
		return convertView;
		
	}
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}
}
