package adapter;

import java.util.List;

import com.manchesterunited.R;

import models.Charts;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ChartsAdapter extends ArrayAdapter<Charts>{
	Activity context;
	int layoutId;
	List<Charts>arr;
	public ChartsAdapter(Activity context, int resource, List<Charts> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvStt=(TextView)convertView.findViewById(R.id.tvSTT);
		TextView tvName=(TextView)convertView.findViewById(R.id.tvClub);
		TextView tvPlay=(TextView)convertView.findViewById(R.id.tvSoTran);
		TextView tvHs=(TextView)convertView.findViewById(R.id.tvHieuSo);
		TextView tvDiem=(TextView)convertView.findViewById(R.id.tvDiemSo);
		int f=Integer.parseInt(arr.get(position).getScoreFor());
		int a=Integer.parseInt(arr.get(position).getScoreAgainst());
		int hs=f-a;
		tvStt.setText(arr.get(position).getIndex());
		tvName.setText(arr.get(position).getNameClub());
		tvPlay.setText(arr.get(position).getPlay());
		tvHs.setText(hs+"");
		tvDiem.setText(arr.get(position).getPoint());
		tvPlay.setText(arr.get(position).getPlay());
		return convertView;
	}
}
