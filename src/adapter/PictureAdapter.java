package adapter;

import java.util.List;

import com.manchesterunited.R;

import lazyloadImage.ImageLoader;
import models.Photos;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class PictureAdapter extends ArrayAdapter<Photos>{
	Activity context;
	int layoutId;
	List<Photos>arr;
	ImageLoader imgLoad;
	private boolean zoomOut =  false;
	public PictureAdapter(Activity context, int resource, List<Photos> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
		imgLoad=new ImageLoader(context.getApplicationContext());
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvTitle=(TextView)convertView.findViewById(R.id.tvPhotoTitle);
		final ImageView imgPicture=(ImageView)convertView.findViewById(R.id.imgPicture);
		
		tvTitle.setText(arr.get(position).getDate()+" : "+arr.get(position).getName());
		imgLoad.DisplayImage(arr.get(position).getImage(), imgPicture, 200);
		imgPicture.buildDrawingCache();
		final Bitmap bm=imgLoad.getImageBitmap(arr.get(position).getImage());
		imgPicture.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				viewImage(bm);
			}
		});
		return convertView;
	}
	private void viewImage(Bitmap bmp)
    {
        final Dialog nagDialog = new Dialog(context,android.R.style.Theme_Black);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); 
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.dialog_full_image);
        final ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.imageView1);
        ivPreview.setImageBitmap(bmp);
        ivPreview.setScaleType(ImageView.ScaleType.FIT_CENTER);
		zoomOut=true;
        ivPreview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(zoomOut==true){
					ivPreview.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
					zoomOut=false;
				}
				else {
					ivPreview.setScaleType(ImageView.ScaleType.FIT_CENTER);
					zoomOut=true;
				}
			}
		});
        nagDialog.show();
        nagDialog.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				 if (keyCode == KeyEvent.KEYCODE_BACK) {
	                    dialog.dismiss();
	                }
	                return true;
			}
		});
    }

}
