package adapter;

import java.util.List;

import com.manchesterunited.R;

import models.Archievement;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArchieveAdapter extends ArrayAdapter<Archievement>{
	Activity context;
	int layoutId;
	List<Archievement>arr;
	public ArchieveAdapter(Activity context, int resource,
			List<Archievement> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.layoutId=resource;
		this.arr=objects;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=context.getLayoutInflater().inflate(layoutId, null);
		TextView tvDate=(TextView)convertView.findViewById(R.id.tvThanhTichDate);
		TextView tvName=(TextView)convertView.findViewById(R.id.tvThanhTichName);
		TextView tvDes=(TextView)convertView.findViewById(R.id.tvNoiDungThanhTich);
		tvDate.setText(arr.get(position).getDate());
		tvName.setText(arr.get(position).getName());
		tvDes.setText(arr.get(position).getDescrible());
		return convertView;
	}
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return false;
	}
}
