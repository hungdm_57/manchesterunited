package models;

public class ItemMenu {
	private int id;
	private int nameRes;
	private String nameString;
	private int iconRes;

	public ItemMenu(int id, int nameRes, int iconRes) {
		this.id = id;
		this.nameRes = nameRes;
		this.iconRes = iconRes;
	}
	public ItemMenu(int id, String nameString, int iconRes) {
		this.id = id;
		this.nameString = nameString;
		this.iconRes = iconRes;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNameRes() {
		return nameRes;
	}

	public void setNameRes(int nameRes) {
		this.nameRes = nameRes;
	}

	public String getNameString() {
		return nameString;
	}

	public void setNameString(String nameString) {
		this.nameString = nameString;
	}

	public int getIconRes() {
		return iconRes;
	}

	public void setIconRes(int iconRes) {
		this.iconRes = iconRes;
	}

}