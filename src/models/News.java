package models;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class News {
	String id;
	String view;
	String date;
	String title;
	String content;

	String url;
	public News(String id,String title, String content,String date,String view,String url){
		this.date=date;
		this.id=id;
		this.view=view;
		this.title=title;
		this.content=content;
		this.url=url;
	}
	
	public String getId(){
		return this.id;
	}
	public String getViews(){
		return this.view;
	}
	public String getDate(){
		return date;
	}
	public String getTitle(){
		return title;
	}
	public String getContent(){
		return this.content;
	}
	public String getUrl(){
		return url;
	}
}
