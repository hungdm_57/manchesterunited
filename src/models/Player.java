package models;

public class Player {
	String id;
	String image;
	String number;
	String firstName;
	String lastName;
	String point;
	String score;
	String assist;
	String win;
	Profile profile;
	public Player(String id,String image,String number,String firstName,String lastName,Profile profile,
			String point,String score,String assist,String win){
		this.id=id;
		this.image=image;
		this.number=number;
		this.firstName=firstName;
		this.lastName=lastName;
		this.profile=profile;
		this.point=point;
		this.score=score;
		this.assist=assist;
		this.win=win;
	}
	public String getId(){
		return id;
	}
	public String getImage(){
		return image;
	}
	public String getNumber(){
		return number;
	}
	public String getFirstName(){
		return firstName;
	}
	public String getLastName(){
		return lastName;
	}
	public String getPoint(){
		return point;
	}
	public String getScore(){
		return score;
	}
	public String getAssist(){
		return assist;
	}
	public String getWin(){
		return win;
	}
	public Profile getProfile(){
		return profile;
	}
 	public class Profile{
		String national;
		String birthday;
		String weight;
		String height;
		String technical;
		public Profile(String national,String birthday,String weight,String height,String technical){
			this.national=national;
			this.birthday=birthday;
			this.weight=weight;
			this.height=height;
			this.technical=technical;
		}
		public String getNational(){
			return national;
		}
		public String getBirthday(){
			return birthday;
		}
		public String getWeight(){
			return weight;
		}
		public String getHeight(){
			return height;
		}
		public String getTechnical(){
			return technical;
		}
	}
}
