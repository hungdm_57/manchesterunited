package models;

import java.util.Date;

import android.graphics.Bitmap;

public class Photos {
	String id;
	String name;
	String date;
	String image;
	public Photos(String id, String name, String date, String url) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.name=name;
		this.image=url;
		this.date=date;
	}
	public String getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public String getDate(){
		return date;
	}
	public String getImage(){
		return image;
	}
}
