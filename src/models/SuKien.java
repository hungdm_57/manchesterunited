package models;

public class SuKien {
	String title;
	String id;
	String date;
	public SuKien(String title,String id,String date){
		this.title=title;
		this.id=id;
		this.date=date;
	}
	public String getTitle(){
		return title;
	}
	public String getId(){
		return id;
	}
	public String getdate(){
		return date;
	}
}
