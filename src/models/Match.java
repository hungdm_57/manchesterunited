package models;

public class Match {
	String id;
	String round;
	String league;
	String home;
	String visitor;
	String city;
	String stadium;
	String date;
	String time;
	String result;
	public Match(String id,String lgue,String rnd,String home,String visit,String city,String stad,String date,String time,String result){
		this.id=id;
		this.league=lgue;
		this.round=rnd;
		this.home=home;
		this.visitor=visit;
		this.city=city;
		this.stadium=stad;
		this.date=date;
		this.time=time;
		this.result=result;
	}
	public Match(String id,String lgue,String rnd,String home,String visit,String city,String stad,String date,String time){
		this.id=id;
		this.league=lgue;
		this.round=rnd;
		this.home=home;
		this.visitor=visit;
		this.city=city;
		this.stadium=stad;
		this.date=date;
		this.time=time;
	}
	public String getId(){
		return id;
	}
	public String getLeague(){
		return league;
	}
	public String getRound(){
		return round;
		
	}
	public String getHome() {
		return home;
	}
	public String getVisitor(){
		return visitor;
	}
	public String getCity(){
		return city;
	}
	public String getStadium(){
		return stadium;
	}
	public String getDate(){
		return date;
	}
	public String getTime(){
		return time;
	}
	public String getResult(){
		return result;
	}
}
