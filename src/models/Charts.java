package models;

public class Charts {
	String index;
	String nameClub;
	String win;
	String lose;
	String draw;
	String play;
	String point;
	String scoreFor;
	String scoreAgainst;
	public Charts(String index,String nameClub,String play,String win,String draw,String lose,String scoreFor,String scoreAgainst,String  point){
		this.index=index;
		this.nameClub=nameClub;
		this.win=win;
		this.lose=lose;
		this.draw=draw;
		this.play=play;
		this.point=point;
		this.scoreFor=scoreFor;
		this.scoreAgainst=scoreAgainst;
	}
	public String getIndex(){
		return index;
	}
	public String getNameClub(){
		return nameClub;
	}
	public String getWin(){
		return win;
	}
	public String getLose(){
		return lose;
	}
	public String getDraw(){
		return draw;
	}
	public String getPlay(){
		return play;
	}
	public String getPoint(){
		return point;
	}
	public String getScoreFor(){
		return scoreFor;
	}
	public String getScoreAgainst(){
		return scoreAgainst;
	}
}
