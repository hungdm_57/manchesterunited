package models;

import android.graphics.Bitmap;

public class ListPlayer {
	String id;
	String image;
	String firstName;
	String number;
	public ListPlayer(String id,String image,String firstName,String number){
		this.id=id;
		this.image=image;
		this.firstName=firstName;
		this.number=number;
	}
	public String getId(){
		return id;
	}
	public String getImage(){
		return image;
	}
	public String getFirstName(){
		return firstName;
	}
	public String getNumber(){
		return number;
	}
}
