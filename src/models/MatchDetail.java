package models;

public class MatchDetail {
	String clubId;
	String time;
	String player;
	String card;
	String result;
	String assist;
	public MatchDetail(String clubId,String time,String player,String result,String assist){
		this.clubId=clubId;
		this.time=time;
		this.player=player;
		this.result=result;
		this.assist=assist;
	}
	public MatchDetail(String clubId,String time,String player,String card){
		this.clubId=clubId;
		this.time=time;
		this.player=player;
		this.card=card;
	}
	public String getClubId(){
		return clubId;
	}
	public String getTime(){
		return time;
	}
	public String getPlayer(){
		return player;
	}
	public String getCard(){
		return card;
	}
	public String getResult(){
		return result;
	}
	public String getAssist(){
		return assist;
	}
}
